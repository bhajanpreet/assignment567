import java.util.Date;

public class HydroBill extends Bill {

	public HydroBill(int billId, Date billDate, String billType, double billAmount, String agencyName,
			int unitsConsumed) {
		super(billId, billDate, billType, billAmount);
		this.agencyName = agencyName;
		this.unitsConsumed = unitsConsumed;
	}

	private String agencyName;

	private int unitsConsumed;

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	public int getUnitsConsumed() {
		return unitsConsumed;
	}

	public void setUnitsConsumed(int unitsConsumed) {
		this.unitsConsumed = unitsConsumed;
	}

	@Override
	public void display() {
		super.display();

		System.out.println("\tAgency Name : " + agencyName);
		System.out.println("\tUnit Consumed :" + unitsConsumed + " Units");

		System.out.println("\t****************************************************");
	}
}
