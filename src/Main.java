import java.util.ArrayList;
import java.util.Date;

public class Main {
	public static void main(String[] args) {
		Customer peter = new Customer(1, "Peter", "Sigurdson", "peter@gmail.com");

		Bill b1 = new HydroBill(1, new Date(), "Hydro", 45.35, "HydroOne", 29);
		Bill b2 = new InternetBill(2, new Date(), "Internet", 56.50, "Rogers", 500);

		ArrayList<Bill> bills = new ArrayList<>();
		bills.add(b2);
		bills.add(b1);
		peter.setBills(bills);

		Customer emad = new Customer(2, "Emad", "Nasrallah", "dr.emad@gmail.com");

		Bill b3 = new HydroBill(1, new Date(), "Hydro", 45.35, "Enbridge", 29);
		Bill b4 = new InternetBill(2, new Date(), "Internet", 56.50, "Rogers", 500);
		Bill b5 = new MobileBill(3, new Date(), "Mobile", 250.69, "Galaxy Samsung Inc.", "Prepaid Talk + Text plan",
				1234567890, 5, 356);
		Bill b6 = new MobileBill(4, new Date(), "Mobile", 300.78, "Apple Inc. iPhone X MAX+", "LTE+3G 9.5GB Promo plan",
				1234567890, 4, 230);

		ArrayList<Bill> bills1 = new ArrayList<>();
		bills1.add(b6);
		bills1.add(b4);
		bills1.add(b5);
		bills1.add(b3);

		emad.setBills(bills1);

		Customer mohammad = new Customer(3, "Mohammad", "Kiani", "mkiani@gmail.com");

		CustomerStore cs = new CustomerStore();

		cs.addCustomer(mohammad);
		cs.addCustomer(peter);
		cs.addCustomer(emad);

		cs.display();

		System.out.println("\n\n\n***********************************************************************");
		System.out.println("************             Searching a customer               ***********");
		System.out.println("***********************************************************************");
		// Search customer
		Customer c1 = cs.getCustomerById(1);
		c1.display();

		System.out.println("\n\n\n***********************************************************************");
		System.out.println("************    Searching a customer that does't exists     ***********");
		System.out.println("***********************************************************************");

		cs.getCustomerById(800);
	}
}
