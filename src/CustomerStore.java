import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CustomerStore implements IDisplay {

	private ArrayList<Customer> customers = new ArrayList<>();

	public void addCustomer(Customer customer) {
		this.customers.add(customer);
	}

	public Customer getCustomerById(int id) {
		Customer c = null;
		for (Customer customer : customers) {
			if (customer.getCustomerId() == id) {
				c = customer;
			}
		}

		if (c == null) {
			System.out.println("No customer with id " + id + " exists.");

		}
		return c;
	}

	@Override
	public void display() {
		Collections.sort(customers, new CustomerComparator());
		for (Customer customer : customers) {
			customer.display();
		}
	}

}

class CustomerComparator implements Comparator<Customer> {

	@Override
	public int compare(Customer b1, Customer b2) {
		if (b1.getCustomerId() < b2.getCustomerId()) {
			return -1;
		} else if (b1.getCustomerId() > b2.getCustomerId()) {
			return 1;
		} else {
			return 0;
		}
	}

}