import java.util.Date;

public class MobileBill extends Bill {

	public MobileBill(int billId, Date billDate, String billType, double billAmount, String mobileModelName,
			String planName, int mobileNumber, double dataUsed, int minutesUsed) {
		super(billId, billDate, billType, billAmount);
		this.mobileModelName = mobileModelName;
		this.planName = planName;
		this.mobileNumber = mobileNumber;
		this.dataUsed = dataUsed;
		this.minutesUsed = minutesUsed;
	}

	private String mobileModelName;

	private String planName;

	private int mobileNumber;

	private double dataUsed;

	private int minutesUsed;

	public String getMobileModelName() {
		return mobileModelName;
	}

	public void setMobileModelName(String mobileModelName) {
		this.mobileModelName = mobileModelName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public int getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(int mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public double getDataUsed() {
		return dataUsed;
	}

	public void setDataUsed(double dataUsed) {
		this.dataUsed = dataUsed;
	}

	public int getMinutesUsed() {
		return minutesUsed;
	}

	public void setMinutesUsed(int minutesUsed) {
		this.minutesUsed = minutesUsed;
	}

	@Override
	public void display() {
		super.display();

		System.out.println("\tManufacturer Name : " + mobileModelName);
		System.out.println("\tPlane Name : " + planName);
		System.out.println("\tMobile Number : +1" + mobileNumber);
		System.out.println("\tInternet Usage : " + dataUsed + " GB");
		System.out.println("\tMinutes Usage : " + minutesUsed + " minutes");

		System.out.println("\t****************************************************");
	}
}
