import java.util.Date;

public class InternetBill extends Bill {

	public InternetBill(int billId, Date billDate, String billType, double billAmount, String providerName,
			double dataUsed) {
		super(billId, billDate, billType, billAmount);
		this.dataUsed = dataUsed;
		this.providerName = providerName;
	}

	private String providerName;

	private double dataUsed;

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public double getDataUsed() {
		return dataUsed;
	}

	public void setDataUsed(double dataUsed) {
		this.dataUsed = dataUsed;
	}

	@Override
	public void display() {
		super.display();

		System.out.println("\tProvider Name : " + providerName);
		System.out.println("\tInternet Usage : " + dataUsed + " GB");

		System.out.println("\t****************************************************");
	}

}
