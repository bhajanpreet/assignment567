import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Customer implements IDisplay {

	public Customer(int customerId, String firstName, String lastName, String emailAddress) {
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailAddress = emailAddress;
	}

	private int customerId;

	private String firstName;

	private String lastName;

	private String emailAddress;

	private ArrayList<Bill> bills;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public ArrayList<Bill> getBills() {
		return bills;
	}

	public void setBills(ArrayList<Bill> bills) {
		this.bills = bills;
	}

	public String getFullName() {
		return firstName + " " + lastName;
	}

	public double getTotalBillToPay() {
		if (bills == null || bills.size() == 0)
			return 0;

		double total = 0;
		for (Bill bill : bills) {
			total += bill.getBillAmount();
		}
		return total;
	}

	@Override
	public void display() {
		System.out.println("Customer Id: " + customerId);
		System.out.println("Customer Full Name: " + this.getFullName());
		System.out.println("Customer Email Id: " + emailAddress);

		if (this.getTotalBillToPay() == 0) {
			System.out.println("\t~~~NOTE : This Customer has no bills");
			System.out.println("\t****************************************************");
			return;
		}
		System.out.println("\t             ----Bill Information----               ");
		System.out.println("\t****************************************************");

		Collections.sort(this.bills, new BillComparator());
		for (Bill bill : this.bills) {
			bill.display();
		}

		System.out.println("\t    Total Amount to Pay: $" + this.getTotalBillToPay());
		System.out.println("\t****************************************************");

	}

}

class BillComparator implements Comparator<Bill> {

	@Override
	public int compare(Bill b1, Bill b2) {
		if (b1.getBillId() < b2.getBillId()) {
			return -1;
		} else if (b1.getBillId() > b2.getBillId()) {
			return 1;
		} else {
			return 0;
		}
	}

}
